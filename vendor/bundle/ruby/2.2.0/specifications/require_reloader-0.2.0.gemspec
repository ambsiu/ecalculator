# -*- encoding: utf-8 -*-
# stub: require_reloader 0.2.0 ruby lib

Gem::Specification.new do |s|
  s.name = "require_reloader"
  s.version = "0.2.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Colin Young", "Huiming Teo"]
  s.date = "2014-01-15"
  s.description = "Auto-reload require files or local gems without restarting server during Rails development."
  s.email = ["me@colinyoung.com", "teohuiming@gmail.com"]
  s.homepage = "https://github.com/teohm/require_reloader"
  s.rubygems_version = "2.4.8"
  s.summary = "Auto-reload require files or local gems without restarting Rails server."

  s.installed_by_version = "2.4.8" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<minitest>, [">= 0"])
    else
      s.add_dependency(%q<minitest>, [">= 0"])
    end
  else
    s.add_dependency(%q<minitest>, [">= 0"])
  end
end
