private

# Copper Cable Size
def cu_cable90(x)
  case
    when x <= 25
      '#14 AWG Copper'
    when x <= 30
      '#12 AWG Copper'
    when x <= 40
      '#10 AWG Copper'
    when x <= 55
      '#8 AWG Copper'
    when x <= 75
      '#6 AWG Copper'
    when x <= 95
      '#4 AWG Copper'
    when x <= 115
      '#3 AWG Copper'
    when x <= 130
      '#2 AWG Copper'
    when x <= 145
      '#1 AWG Copper'
    when x <= 170
      '#1/0 AWG Copper'
    when x <= 195
      '#2/0 AWG Copper'
    when x <= 225
      '#3/0 AWG Copper'
    when x <= 260
      '#4/0 AWG Copper'
    when x <= 290
      '250 Kcmil Copper'
    when x <= 320
      '300 Kcmil Copper'
    when x <= 350
      '350 Kcmil Copper'
    when x <= 380
      '400 Kcmil Copper'
    when x <= 430
      '500 Kcmil Copper'
    when x <= 475
      '600 Kcmil Copper'
    else
      'Error'
  end
end

# Aluminum Cable Size

def al_cable90(x)
  case
    when x <= 25
      '#12 AWG Aluminum'
    when x <= 35
      '#10 AWG Aluminum'
    when x <= 45
      '#8 AWG Aluminum'
    when x <= 55
      '#6 AWG Aluminum'
    when x <= 75
      '#4 AWG Aluminum'
    when x <= 85
      '#3 AWG Aluminum'
    when x <= 100
      '#2 AWG Aluminum'
    when x <= 115
      '#1 AWG Aluminum'
    when x <= 135
      '#1/0 AWG Aluminum'
    when x <= 150
      '#2/0 AWG Aluminum'
    when x <= 175
      '#3/0 AWG Aluminum'
    when x <= 205
      '#4/0 AWG Aluminum'
    when x <= 230
      '250 Kcmil Aluminum'
    when x <= 260
      '300 Kcmil Aluminum'
    when x <= 280
      '350 Kcmil Aluminum'
    when x <= 305
      '400 Kcmil Aluminum'
    when x <= 350
      '500 Kcmil Aluminum'
    when x <= 385
      '600 Kcmil Aluminum'
    else
      'Error'
  end
end