# For both House and Apt Calculations

private


# Apartment Basic Load Calculation
def aptbasic(area)
  if area%45 == 0
    n = area / 45
  else
    n = (area / 45) + 1
  end
  x = n
  aptbasic_result = 0
  while n > 0
    if n >= (x)
      aptbasic_result += 3500
      n -= 1
    elsif n >= (x - 1)
      aptbasic_result += 1500
      n -= 1
    else
      aptbasic_result += 1000
      n -= 2
    end
  end
  aptbasic_result
end

# House Basic Load Calculation
def housebasic(area)
  if area%90 == 0
    n = area / 90
  else
    n = (area / 90) + 1
  end

  x = n
  housebasic_result = 0

  while n > 0
    if n >= (x)
      housebasic_result += 5000
      n -= 1
    else
      housebasic_result += 1000
      n -= 1
    end
  end
  return housebasic_result
end

# Main Range Calculation

def range(range_power)
  range_watts = 0
  range_extra = 0
  range_extra = (range_power - 12000) * 0.40 if range_power > 12000
  range_watts += 6000 if range_power != 0
  return (range_watts + range_extra)
end

# Additional Range Calculation
def add_range(arp, erp_result)
  if erp_result != 0
    calculate_extra_load(arp, erp_result).to_i
  else
    0
  end
end

# Electric Space Heating Calculation
def space_heat(eshp, acp, interlock)
  eshp_result = (10000 + (eshp - 10000) * 0.75).to_i
  if eshp_result > acp || interlock != 1
    if eshp <= 10000
      return eshp
    else
      return eshp_result
    end
  end
end

# Extra Load Calculation (Exceed 1.5kW)

def calculate_extra_load(extra_load_power, erp_result)
  if erp_result != 0 && extra_load_power > 1500
    return (extra_load_power * 0.25).to_i
  elsif erp_result != 0 && extra_load_power <= 1500
    return 0
  else
    return 6000 + (extra_load_power * 0.25).to_i if extra_load_power != 0 && extra_load_power > 1500
  end
end

# Total Load Demand Calculation

def total_demand(size_result, erp_result, arp_result, eshp_result, acp_result, hwtp_result, wmp_result, cdp_result, evp_result, load1_result, load2_result, load3_result)
  (size_result + erp_result + arp_result + eshp_result + acp_result + hwtp_result + wmp_result + cdp_result + evp_result + load1_result + load2_result + load3_result)
end

def calculated_amp(voltage, total_demand)
  (total_demand / voltage).to_i
end