private

def voltage_drop(voltage, maxdrop, current, conductor, conductor_temp)

  td3_1 = YAML.load(File.read("yaml/td3_1.yml"))
  td3_2 = YAML.load(File.read("yaml/td3_2.yml"))
  table2 = YAML.load(File.read("yaml/table2.yml"))

  if voltage != 0 && maxdrop != 0.0 && current != 0.0 && conductor != 0 && conductor_temp != 0

    if current > table2[conductor_temp][conductor] ||  current > td3_1[conductor].invert.values.last
      'too_high'
    elsif current < td3_1[conductor].invert.values.first
      'too_low'
    else
      ampacity_percent = ((current / table2[conductor_temp][conductor]).round(-1)) * 100
      ampacity_percent = 40 if ampacity_percent < 40
      correct_factor = td3_2[ampacity_percent][conductor_temp]

      distance = (td3_1[conductor].invert.select { |k, v| v >= current }).first.first
      (distance * maxdrop * correct_factor * (voltage/120)).round(1)
    end
  else
    0
  end

end