require 'sinatra'
require 'bundler/setup'
require 'yaml'
require File.join(File.dirname(__FILE__), "formulas")
require File.join(File.dirname(__FILE__), "vdrop")

get '/' do
  erb :welcome, layout: :layout
end

get '/apt' do
  erb(:apt, {layout: :layout})
end

get '/house' do
  erb :house, layout: :layout
end

get '/voltage_drop' do
  erb :voltage_drop, layout: :layout
end

get '/contact' do
  erb :contact, layout: :layout
end


post '/housedemand' do

  # House Basic Load Calculation
  @floor_area = params["floor_area"].to_i
  @basement_area = params["basement_area"].to_i
  @house_area = @floor_area + @basement_area
  @demand_area = @floor_area + (@basement_area * 0.75)
  @size_result = housebasic(@demand_area).to_i

  # Range Calculation
  @erp = params["erp"].to_i
  @erp_result = range(@erp).to_i

  # Additional Range Calculation
  @arp = params["arp"].to_i
  @arp_result = add_range(@arp, @erp_result).to_i

  # Electric Space Heating Calculation
  @eshp = params["eshp"].to_i
  @acp = params["acp"].to_i
  @interlock = params["interlock"].to_i
  @eshp_result = space_heat(@eshp, @acp, @interlock).to_i

  # Air Conditioning Calculation
  @acp_result = 0
  if @eshp_result <= @acp || @interlock != 1
    @acp_result = @acp
  end

  # Tankless Water Heaters
  @tankless_result = params["tankless_heat"].to_i

  # Hot Water Tank Calculation
  @hwtp = params["hwtp"].to_i
  @hwtp_result = calculate_extra_load(@hwtp, @erp_result).to_i

  # Washing Machine Calculation
  @wmp = params["wmp"].to_i
  @wmp_result = calculate_extra_load(@wmp, @erp_result).to_i

  # Clothes Dryer Calculation
  @cdp = params["cdp"].to_i
  @cdp_result = calculate_extra_load(@cdp, @erp_result).to_i

  # EV Charging Calculation
  @evp = params["evp"].to_i
  @evp_result = @evp

  # load 1 Calculation
  @load1 = params["load1"].to_i
  @load1_result = calculate_extra_load(@load1, @erp_result).to_i

  # Load 2 Calculation
  @load2 = params["load2"].to_i
  @load2_result = calculate_extra_load(@load2, @erp_result).to_i

  # Load 3 Calculation
  @load3 = params["load3"].to_i
  @load3_result = calculate_extra_load(@load3, @erp_result).to_i

  # Total Demand Power Calculation
  @total_power = total_demand(@size_result, @erp_result, @arp_result, @eshp_result, @acp_result, @hwtp_result, @wmp_result, @cdp_result, @evp_result, @load1_result, @load2_result, @load3_result).to_i

  # Calculated Ampacity
  @calculated_amp = calculated_amp(240, @total_power).to_i


  erb(:house, {layout: :layout})

end

# APARTMENT

post '/aptdemand' do

  # Apartment Basic Load Calculation
  @aptarea = params["size"].to_i
  @size_result = aptbasic(@aptarea).to_i

  # Range Calculation
  @erp = params["erp"].to_i
  @erp_result = range(@erp).to_i

  # Additional Range Calculation
  @arp = params["arp"].to_i
  @arp_result = add_range(@arp, @erp_result).to_i

  # Electric Space Heating Calculation
  @eshp = params["eshp"].to_i
  @acp = params["acp"].to_i
  @interlock = params["interlock"].to_i
  @eshp_result = space_heat(@eshp, @acp, @interlock).to_i

  # Air Conditioning Calculation
  @acp_result = 0
  if @eshp <= @acp || @interlock != 1
      @acp_result = @acp
  end

  # Tankless Water Heaters
  @tankless_result = params["tankless_heat"].to_i

  # Hot Water Tank Calculation
  @hwtp = params["hwtp"].to_i
  @hwtp_result = calculate_extra_load(@hwtp, @erp_result).to_i

  # Washing Machine Calculation
  @wmp = params["wmp"].to_i
  @wmp_result = calculate_extra_load(@wmp, @erp_result).to_i

  # Clothes Dryer Calculation
  @cdp = params["cdp"].to_i
  @cdp_result = calculate_extra_load(@cdp, @erp_result).to_i

  # load 1 Calculation
  @load1 = params["load1"].to_i
  @load1_result = calculate_extra_load(@load1, @erp_result).to_i

  # Load 2 Calculation
  @load2 = params["load2"].to_i
  @load2_result = calculate_extra_load(@load2, @erp_result).to_i

  # Load 3 Calculation
  @load3 = params["load3"].to_i
  @load3_result = calculate_extra_load(@load3, @erp_result).to_i

  # Total Demand Power Calculation
  @total_power = total_demand(@size_result, @erp_result, @arp_result, @eshp_result, @acp_result, @hwtp_result, @wmp_result, @cdp_result, 0, @load1_result, @load2_result, @load3_result).to_i

  # Calculated Ampacity
  @calculated_amp = calculated_amp(240, @total_power).to_i



  erb(:apt, {layout: :layout})


end

# Voltage Drop

post '/voltage_drop' do
  @voltage = params["voltage"].to_i
  @maxdrop = params["maxdrop"].to_f
  @current = params["current"].to_f
  @conductor = params["conductor"].to_i
  @conductor_temp = params["conductor_temp"].to_i

  @vdrop_result = voltage_drop(@voltage, @maxdrop, @current, @conductor, @conductor_temp)

  erb :voltage_drop, layout: :layout
end